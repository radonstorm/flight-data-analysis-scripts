import psycopg2
import csv
from sys import argv
from datetime import datetime, timedelta
from pathlib import Path
import networkx as nx
import matplotlib.pyplot as plt
# database config file
from database_config import DATABASE
# Basemap must be installed using conda
# conda install -c conda-forge basemap
# if anaconda is not being used, disable the import of the following module using '--disable-netmap'
# if not installed the script will show a message and continue with netmap features disabled
disable_netmap = True
if '--disable-netmap' not in argv:
    try:
        from mpl_toolkits.basemap import Basemap
        disable_netmap = False
    except ImportError:
        disable_netmap = True
        print('Basecamp not installed, use \'--disable-netmap\' next time to avoid this warning')

connection = None
cursor = None
try:
    connection = psycopg2.connect(database=DATABASE['DATABASE'], user=DATABASE['USER'], password=DATABASE['PASSWORD'], host=DATABASE['HOST'])
    # test the connection
    cursor = connection.cursor()
except psycopg2.DatabaseError as err:
    if connection is not None:
        connection.close()
    print(err)

airport_data = list()
with open('airport.csv', 'r') as file:
    reader = csv.DictReader(file, delimiter=',')
    for airport in reader:
        airport['latitude'] = float(airport['latitude'])
        airport['longitude'] = float(airport['longitude'])
        airport_data.append(airport)

# do some airport statistics
for airport in airport_data:
    # get inbound flights
    cursor.execute('SELECT COUNT(*) FROM flights WHERE destination_id = %s', (airport['iata_code'],))
    airport['inbound_flights'] = cursor.fetchone()[0]
    # get outbound flights
    cursor.execute('SELECT COUNT(*) FROM flights WHERE origin_id = %s', (airport['iata_code'],))
    airport['outbound_fights'] = cursor.fetchone()[0]
    # get total inbound seats
    cursor.execute("""SELECT SUM(num_seats) FROM aircraft WHERE id IN
        (SELECT aircraft_id FROM flightinfo WHERE flight_id IN
        (SELECT id FROM flights WHERE destination_id = %s)) AND num_seats IS NOT NULL""", (airport['iata_code'],))
    airport['total_inbound_seats'] = cursor.fetchone()[0]
    # get total outbound seats
    cursor.execute("""SELECT SUM(num_seats) FROM aircraft WHERE id IN
        (SELECT aircraft_id FROM flightinfo WHERE flight_id IN
        (SELECT id FROM flights WHERE origin_id = %s)) AND num_seats IS NOT NULL""", (airport['iata_code'],))
    airport['total_outbound_seats'] = cursor.fetchone()[0]

# do some route statistics
cursor.execute('SELECT DISTINCT origin_id, destination_id FROM flights ORDER BY origin_id')
routes = cursor.fetchall()
for route in routes:
    origin = route[0]
    destination = route[1]
    cursor.execute('SELECT COUNT(*) FROM flights WHERE origin_id = %s AND destination_id = %s', (origin, destination))
    num_flights = cursor.fetchone()[0]
    if num_flights is None:
        num_flights = 0
    cursor.execute("""SELECT SUM(num_seats) FROM aircraft WHERE id IN
        (SELECT aircraft_id FROM flightinfo WHERE flight_id IN
        (SELECT id FROM flights WHERE origin_id = %s AND destination_id = %s)) AND num_seats IS NOT NULL""", (origin, destination))
    num_seats = cursor.fetchone()[0]
    if num_seats is None:
        num_seats = 0
    routes[routes.index(route)] = {
        'origin': origin,
        'destination': destination,
        'num_flights': num_flights,
        'num_seats': num_seats
    }

with open('airport-statistics.csv', 'w') as file:
    writer = csv.DictWriter(file, fieldnames=airport_data[0].keys())
    writer.writeheader()
    for airport in airport_data:
        writer.writerow(airport)

with open('route-statistics.csv', 'w') as file:
    writer = csv.DictWriter(file, fieldnames=routes[0].keys())
    writer.writeheader()
    combined_routes = dict()
    # go through each route, combine if needed
    for route in routes:
        # encountered the route already as the inverse, merge numbers
        if f'{route["destination"]}-{route["origin"]}' in combined_routes.keys():
            key = f'{route["destination"]}-{route["origin"]}'
            combined_routes[key]['num_flights'] += route['num_flights']
            combined_routes[key]['num_seats'] += route['num_seats']
        # not encountered yet, add as 'normal'
        else:
            key = f'{route["origin"]}-{route["destination"]}'
            combined_routes[key] = route
    writer.writerows(combined_routes.values())

# build graph
# start with generating a map
# map dimensions
if not disable_netmap:
    lower_left_long = 90
    lower_left_lat = -50
    upper_right_long = 170
    upper_right_lat = 0
    m = Basemap(
        projection='merc',
        llcrnrlon=lower_left_long,
        llcrnrlat=lower_left_lat,
        urcrnrlon=upper_right_long,
        urcrnrlat=upper_right_lat,
        resolution='l'
    )
    # create blank directed graph
    flight_network = nx.DiGraph()
    # add nodes to network
    for airport in airport_data:
        # ensure ERR point and points outside map are not added
        if airport['iata_code'] != 'ERR' and lower_left_lat <= airport['latitude'] <= upper_right_lat and lower_left_long <= airport['longitude'] <= upper_right_long:
            # transpose latitude and longitude to map coordinates
            mx, my = m(airport['longitude'], airport['latitude'])
            flight_network.add_node(airport['iata_code'], pos=(mx, my))
    # add all routes as edges
    for edge in routes:
        # ensure ERR and edges outside map are not added
        if 'ERR' not in (edge['origin'], edge['destination']) and edge['origin'] in flight_network.nodes and edge['destination'] in flight_network.nodes:
            flight_network.add_edge(edge['origin'], edge['destination'], weight=edge['num_flights'])
    # print out degree of each node
    nodes_sorted = {}
    for node in flight_network.nodes:
        nodes_sorted[node] = flight_network.degree(node)
    with open('route-statistics.txt', 'w') as file:
        nodes_sorted = dict(sorted(nodes_sorted.items(), reverse=True, key=lambda x: x[1]))
        file.write('Node degrees:\n')
        for key in nodes_sorted:
            file.write(f'{key}: {nodes_sorted[key]}\n')
    # draw the map
    nx.draw(flight_network, nx.get_node_attributes(flight_network, 'pos'), with_labels=True, node_size=10, width=0.1)
    m.drawcoastlines(linewidth=0.3)
    m.drawstates(linewidth=0.3)
    plt.savefig('route-map.png')
    plt.show()
