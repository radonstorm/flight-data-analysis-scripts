# Flight Data Analysis Scripts

This repo contains scripts and tools for data analysis of flight data obtained from the Curtin Flight Data project

The scripts are designed to be run with an Anaconda 3 instance specified in the environment.yml file. Create an environment with the following command

```
conda env create -f environment.yml
```

However a requirements.txt file is also provided if you prefer to install using pip, or prefer to not use Anaconda(*)

```
pip install --user -r requirements.txt
```

(*) note, basemap package can only be installed with Anaconda, network diagram will not be generated when running `route_statistics.py`

## generate_heatmap.py

This script is used to generate a heatmap CSV file from the complete airfare dataset


```
python generate_heatmap.py [-fn flight_number] [--departure_range DATE,DATE] [--tc_range DATE,DATE] [-i input_file] [-o output_file]
```

Running the script will prompt the user for a flight number, fare class and ranges for the departure times of flights and data collection times.
By default it looks for a `'allairfarequery.csv'` file for data input and will output to `'heatmap.csv'`.
These options (with the exception of fare class) can be set by optional command-line flags.


### Issues
Currently the `time_collected` field is accurate to the hour it was collected. Some scrape sessions may have been delayed and will result in some data being split across two or more `time_collected` columns.

## route_statistics.py

This script is used to generate statistics for an analysis of the Western Australian air travel network

```
python route_statistics.py
```

The script runs automatically, but requires access to a locally setup flight data database and a copy of airport.csv.
Anaconda is also required in order to install the packages needed to generate the network diagram. If anaconda is not used or the required packages are not installed, the script can be run with `--disable-netmap` flag enabled.

Database connection settings must be stored in a file called `database_config.py` with the following structure:

```
DATABASE = {
    'DATABASE': 'flight_scraper',  # database name
    'USER': 'USERNAME',  # username
    'PASSWORD': 'PASSWORD',  # password for user
    'HOST': 'localhost'
}
```