import csv
from datetime import datetime

flight_data_fn = 'allflightstatusquery.csv'

airlines = {
    'Qantas': 0,
    'Jetstar': 0,
    'Virgin': 0
}
fmt_str = '%Y-%m-%d %H:%M:%S'

lower_bound = datetime(2020, 12, 1)
upper_bound = datetime(2020, 12, 31)

with open(flight_data_fn, 'r') as file:
    reader = csv.DictReader(file)
    for row in reader:
        departure_time = datetime.strptime(row['departure_time'], fmt_str)
        if lower_bound <= departure_time <= upper_bound:
            for airline in airlines:
                if airline == row['airline']:
                    if row['aircraft_seats'] != '0' and row['aircraft_seats'] is not None and row['aircraft_seats'] != '':
                        airlines[airline] += int(row['aircraft_seats'])

print(airlines)
