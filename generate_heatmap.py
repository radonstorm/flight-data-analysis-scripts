"""
generates a csv file for use in a heatmap
take flight number as argument
"""

import pandas as pd
import argparse
from datetime import datetime


def generate_heatmap(input_file, output_file, flight_number, dep_range=None, tc_range=None):
    # format string for database datetimes
    fmt_str = '%Y-%m-%d %H:%M:%S'
    # ensure flight_number is in uppercase
    flight_number = flight_number.upper()

    # read in WHOLE csv file
    # header starts on the second line of the file, to ignore generated timestamp comment
    flights = pd.read_csv(input_file, header=1)
    # filter to one flight number
    query = pd.DataFrame()
    while query.empty:
        query = flights.query('flight_number == @flight_number').copy()
        if query.empty:
            print(f'No data found with flight number of {flight_number}\nPlease enter a new flight number')
            flight_number = input('> ').upper()
    # ask the user what airfare class they want
    classes = pd.unique(query['fare_class'])
    class_choice = -1
    ii = 1
    for option in classes:
        print(f'{ii}: {option}')
        ii += 1
    while class_choice == -1:
        print('Select a class')
        class_choice = int(input('> ')) - 1
        if class_choice < 0 or class_choice >= len(classes):
            print('Invalid choice')
            class_choice = -1
    choice = classes[class_choice]
    # ask user to enter a range for time_collected
    if tc_range is None:
        print('Enter \'N\' to ignore data collection date')
        start_date_tc = input('Enter a start date for data collection (YYYY-MM-DD) > ').strip().upper()
        end_date_tc = input('Enter a end date for data collection (YYYY-MM-DD) > ').strip().upper()
        tc_range = start_date_tc + ',' + end_date_tc
        if start_date_tc == 'N' or end_date_tc == 'N':
            tc_range = '2020-01-01,2030-01-01'
    # ask user to enter a range for departure dates
    if dep_range is None:
        print('Enter \'N\' to ignore flight departure dates')
        start_date_dep = input('Enter a start date for flight departure date (YYYY-MM-DD) > ').strip().upper()
        end_date_dep = input('Enter a end date for flight departure date (YYYY-MM-DD) > ').strip().upper()
        dep_range = start_date_dep + ',' + end_date_dep
        if start_date_dep == 'N' or end_date_dep == 'N':
            dep_range = '2020-01-01,2030-01-01'
    # convert all the dates the user inputs to datetime objects so time comparisons can be done
    start_date_tc = datetime.strptime(tc_range.split(',')[0] + ' 00:00:00', fmt_str)
    end_date_tc = datetime.strptime(tc_range.split(',')[1] + ' 00:00:00', fmt_str)
    start_date_dep = datetime.strptime(dep_range.split(',')[0] + ' 00:00:00', fmt_str)
    end_date_dep = datetime.strptime(dep_range.split(',')[1] + ' 00:00:00', fmt_str)

    # convert times to datetime from str
    # use apply here to have time_collected drop the minutes for a better coalated heatmap
    query.loc[:, 'time_collected'] = query.loc[:, 'time_collected'].apply(convert_to_datetime)
    query.loc[:, 'departure_time'] = pd.to_datetime(query.loc[:, 'departure_time'], format=fmt_str)
    query.loc[:, 'arrival_time'] = pd.to_datetime(query.loc[:, 'arrival_time'], format=fmt_str)

    # filter the data to the correct class and departure date
    query = query.query('fare_class == @choice and @start_date_dep <= departure_time <= @end_date_dep and @start_date_tc <= time_collected <= @end_date_tc').copy()
    # sort the query by time collected, grouping by departure time
    query = query.sort_values(by=['departure_time', 'time_collected'], ascending=True)

    # find the axis of heatmap dataframe
    departure_times = pd.unique(query.sort_values(by=['departure_time'], ascending=True)['departure_time'])
    times_collected = pd.unique(query.sort_values(by=['time_collected'], ascending=True)['time_collected'])

    # create heatmap dataframe using departure_times and times_collected as axis
    heatmap = pd.DataFrame(index=departure_times, columns=times_collected)
    # loop through each flight in departure_times
    for time in departure_times:
        # find the fares for the specific flight
        fares = pd.DataFrame(data=query.query('departure_time == @time'))
        # create Series using time_collected as an index
        asdf = pd.Series(data=fares['fare'].values, index=fares['time_collected'])
        # add to heatmap
        heatmap.loc[time] = asdf
    # write heatmap out to CSV file
    heatmap.to_csv(output_file)


def convert_to_datetime(time_string):
    fmt_str = '%Y-%m-%d %H:%M:%S'
    time_string = time_string.split('.')[0]
    time_string = datetime.strptime(time_string, fmt_str)
    return time_string.replace(minute=0, second=0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate a CSV file to generate a heatmap')
    parser.add_argument('-fn', metavar='flight_number', default=None, help='Flight number to search for')
    parser.add_argument('-i', metavar='input_file', default='allairfarequery.csv', help='Input data file path')
    parser.add_argument('-o', metavar='output_file', default='heatmap.csv', help='Output results to file')
    parser.add_argument('--departure_range', metavar='DATE,DATE', default=None, help='Departure date range (YYYY-MM-DD,YYYY-MM-DD)')
    parser.add_argument('--tc_range', metavar='DATE,DATE', default=None, help='Time collected date range (YYYY-MM-DD,YYYY-MM-DD)')
    args = parser.parse_args()
    if args.fn is None:
        args.fn = input('Please input a flight number > ').upper()
    generate_heatmap(args.i, args.o, args.fn.upper(), args.departure_range, args.tc_range)
