import csv
from datetime import datetime

# datetime format string
fmt_str = '%Y-%m-%d %H:%M:%S'
# csv fields to filter with
filters = ['departure_time', 'time_collected']

filename = input('Enter file name> ')
start_date = input('Enter start date (YYYY-MM-DD HH:MM:SS) ').strip()
finish_date = input('Enter finish date (YYYY-MM-DD HH:MM:SS) ').strip()
# prompt user for filter to use
choice_prompt = 'Enter filter type: \n'
for filter in range(0, len(filters)):
    choice_prompt += f'{filter}: {filters[filter]} \n'
filter_choice = int(input(choice_prompt))
filter = filters[filter_choice]
# create start and finish datetimes
start_date = datetime.strptime(start_date, fmt_str)
finish_date = datetime.strptime(finish_date, fmt_str)
# open airfare data
with open(filename, 'r') as file:
    reader = csv.DictReader(file)
    # create new filtered data csv file
    new_csv = open('filtered-csv.csv', 'w')
    writer = csv.DictWriter(new_csv, reader.fieldnames)
    writer.writeheader()
    # loop over every airfare record
    for row in reader:
        # find time to compare with
        time = datetime.strptime(row[filter].split('.')[0], fmt_str)
        # if the time is within the bounds set by the user write the airfare to the new csv file
        if start_date <= time <= finish_date:
            writer.writerow(row)
    # close and save
    new_csv.close()
